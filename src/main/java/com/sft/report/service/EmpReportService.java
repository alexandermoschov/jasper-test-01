package com.sft.report.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.sft.report.dto.Employee;

@Service
public class EmpReportService {

	public static List<Employee> empList = Arrays.asList(
			new Employee(1, "Sandeep", "Data Matrix", "Front-end Developer", 20000),
			new Employee(2, "Prince", "Genpact", "Consultant", 40000),
			new Employee(3, "Gaurav", "Silver Touch ", "Sr. Java Engineer", 47000),
			new Employee(4, "Abhinav", "Akal Info Sys", "CTO", 700000));

    @Autowired
    private ResourceService resourceService;
    
    @Autowired
    private ReportTemplateService reportTemplateService;
	
	public String generateReport() {
		try {
            String reportPath = "reports";
            Path reportOutPath =  Paths.get("target/out");
            Files.createDirectories(reportOutPath);

            
            Resource resource = resourceService.find("reports/employee-rpt.jrxml");
		    
		    System.out.println(resource.getFile());

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(resource.getFile().getAbsolutePath());

			// Get your data source
			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(empList);

			// Add parameters
			Map<String, Object> parameters = new HashMap<>();

			parameters.put("createdBy", "Websparrow.org");

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);

			// Export the report to a PDF file
			JasperExportManager.exportReportToPdfFile(jasperPrint, reportOutPath.resolve("emp-rpt.pdf").toString());

			System.out.println("Done");

			return "Report successfully generated @path= " + reportPath;

		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}



    public String generateReportSimple() {
        try {
            Path outDirPath =  Paths.get("target/out");
            Files.createDirectories(outDirPath);
            Path outPath = outDirPath.resolve("emp-rpt.pdf");
            

            // Compile the Jasper report from .jrxml to .japser
            JasperReport jasperReport = reportTemplateService.loadTemplate("employee-rpt");

            // Get your data source
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(EmpReportService.empList);

            // Add parameters
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("createdBy", "Websparrow.org");

            // Fill the report
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource);

            // Export the report to a PDF file
            JasperExportManager.exportReportToPdfFile(jasperPrint, outPath.toString());

            System.out.println("Done");

            return "Report successfully generated @path= " + outPath.toAbsolutePath();

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }	

}
