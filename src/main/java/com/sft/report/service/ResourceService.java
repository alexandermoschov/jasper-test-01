package com.sft.report.service;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import static org.apache.commons.lang3.StringUtils.startsWith;

/**
 * Provides the functionality to work with local resources
 * 
 * @author Alex Moshchev
 */
@Service
public class ResourceService {

    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    public Resource find(final String path) throws IOException {
        Resource result = resourceLoader.getResource("classpath:" + (!startsWith(path, "/") ? "/" : "") + path);
        if (!result.exists())
            result = resourceLoader.getResource("file://" + path);

        if (!result.exists())
            throw new IOException("Resource \"" + path + "\" not found");
        return result;
    }

    public byte[] load(String path) throws IOException {
        final Resource resource = find(path);
        Assert.notNull(resource, "The resource must be not null");
        try (InputStream in = resource.getInputStream()) {
            return IOUtils.toByteArray(in);
        }
    }

    public String loadAsString(String path) throws IOException {
        final Resource resource = find(path);
        Assert.notNull(resource, "The resource must be not null");
        try (InputStream in = resource.getInputStream()) {
            return IOUtils.toString(in, "UTF-8");
        }
    }

    public InputStream inputStream(String path) throws IOException {
        final Resource resource = find(path);
        Assert.notNull(resource, "The resource must be not null");
        return resource.getInputStream();
    }

    /**
     * loadResources(classpath*:../../dir/*.txt);
     * */
    public Resource[] loadResources(final String pattern) throws IOException {
        return ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources(pattern);
    }
    
}
