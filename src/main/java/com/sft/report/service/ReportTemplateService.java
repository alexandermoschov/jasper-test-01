package com.sft.report.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.SimpleCsvReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

/**
 * Provides the functionality to work with Jasper ReportTemplate
 * 
 * @author Alex Moshchev
 */
@Slf4j
@Service
public class ReportTemplateService {

    @Autowired
    private ResourceService resourceService;

    private JRVirtualizer virtualizer;
    
    private boolean cleanStorage = false;

    @PostConstruct
    public void init() throws IOException {
        log.info("Initializing ReportTemplateManager ...");
        doInit();
    }

    @PreDestroy
    public void onDestroy() throws IOException {
        if (virtualizer != null) {
            virtualizer.cleanup();
        }
    }
    
    private Path getReportTempLocation() {
        // ${java.io.tmpdir}/buportal/reports/vfs
        return getTemplateLocation().resolve("vfs");
    }
    
    
    public Path getTemplateLocation() {
        //  storage-location: file:${user.home}/juliuskrah.com
        // report-data-directory: ${java.io.tmpdir}/buportal/reports
        //return "${java.io.tmpdir}/buportal/reports";
        return Paths.get("tmp/reports");
    }

    private void doInit() throws IOException {
        final Path templateLocation = getTemplateLocation();
        
        log.info("Using the template location {} ...", templateLocation.toAbsolutePath());
        Files.createDirectories(templateLocation);
        if (cleanStorage) {
            FileUtils.cleanDirectory(templateLocation.toFile());
        }

        final Charset charset = StandardCharsets.UTF_8;
        final Resource[] resources = resourceService.loadResources("classpath*:/reports/*.*");
        for (Resource resource : resources) {
            log.debug("Found " + resource.getFilename());
            final String fileNameExt = FilenameUtils.getExtension(resource.getFilename());
            
            try (InputStream in = resource.getInputStream();
                 OutputStream out = new FileOutputStream(templateLocation.resolve(resource.getFilename()).toFile())) {
                if ("jrxml".equals(fileNameExt)) {
                    String content = IOUtils.toString(in, charset);
                    content = content.replace("\"styles.jrtx\"", "$P{TEMPLATE_LOCATION} + \"/styles.jrtx\"");
                    IOUtils.write(content, out, charset);
                }
                else {
                    FileCopyUtils.copy(in, out);
                }
            }
        }

        final Path reportTempLocation = getReportTempLocation();
        Files.createDirectories(reportTempLocation);
        this.virtualizer = new JRFileVirtualizer(1000, reportTempLocation.toFile().getAbsolutePath());
    }

    private Path getPath(final String name, final String ext) {
        return getTemplateLocation().resolve(name + "." + ext);
    }

    public JasperReport loadTemplate(final String templateName) throws JRException {
        log.debug("Template path for {} : {}", templateName, "");
        final JasperReport jasperReport;
        
        final Path jrTemplatePath = getPath(templateName, "jasper");
        if (Files.exists(jrTemplatePath)) {
            jasperReport = (JasperReport) JRLoader.loadObject(jrTemplatePath.toFile());
        }
        // Compile report from source and save
        else {
            final Path templatePath = getPath(templateName, "jrxml");

            log.info("Compile report {}", templatePath);
            jasperReport = JasperCompileManager.compileReport(templatePath.toFile().getAbsolutePath());

            log.info("Save compiled report {} to {}", templatePath,  jrTemplatePath);
            JRSaver.saveObject(jasperReport, jrTemplatePath.toFile());
        }

        return jasperReport;
    }
    
    public File generatePdf(final JasperPrint jasperPrint, final File out) throws JRException {
        Assert.notNull(jasperPrint, "The JasperPrint must be not null");
        Assert.notNull(out, "The out must be not null");

        final JRPdfExporter exporter = new JRPdfExporter();
        
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
         
        final SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);
         
        final SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor("baeldung");
        exportConfig.setEncrypted(true);
        exportConfig.setAllowedPermissionsHint("PRINTING");
         
        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);
         
        exporter.exportReport();

        return out;
    }
    
    public File generateCsv(final JasperPrint jasperPrint, final File out) throws JRException {
        Assert.notNull(jasperPrint, "The JasperPrint must be not null");
        Assert.notNull(out, "The out must be not null");

        final JRCsvExporter exporter = new JRCsvExporter();
        
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(out));
         
        final SimpleCsvReportConfiguration reportConfig = new SimpleCsvReportConfiguration();
        exporter.setConfiguration(reportConfig);
         
        exporter.exportReport();

        return out;
    }
    
    public JRVirtualizer getVirtualizer() throws IOException {
        final Path directory = getReportTempLocation();
        Files.createDirectories(directory);
        return new JRFileVirtualizer(1000, directory.toFile().getAbsolutePath());
    }


}
