package com.sft.report.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.SimpleCsvReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.emptyMap;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;

@Service
public class ReportService {
    private static final Logger log = LoggerFactory.getLogger(ReportService.class);

    static final String RECONCILIATION_SRV_TEMPLATE_NAME = "reconciliation_srv";
    static final String RECONCILIATION_SRV_DAT_TEMPLATE_NAME = "reconciliation_srv_dat";
    static final String RECONCILIATION_DB_TEMPLATE_NAME = "reconciliation_db";
    static final String RECONCILIATION_DB_DAT_TEMPLATE_NAME = "reconciliation_db_dat";
    
    static final String DB_RECONCILIATION_METRICS_QUERY =
            "select nvl(metric_name,'-')||'_'||nvl(entity_type,'-')||'_'||nvl(environment,'-')||'_'||nvl(gxp,'-') key, value from table ( ui.db_reconciliation(?))";
    static final String SRV_RECONCILIATION_METRICS_QUERY =
            "select nvl(metric_name,'-')||'_'||nvl(entity_type,'-')||'_'||nvl(environment,'-')||'_'||nvl(gxp,'-') key, value from table ( ui.srv_reconciliation(?))";

    static final String REPORT_MODE_PARM_NAME = "REPORT_MODE";
    static final String REPORT_DATE_PARM_NAME = "REPORT_DATE";
    static final String REPORT_NAME_PARM_NAME = "REPORT_NAME";
    static final String GENERATED_BY_PARM_NAME = "GENERATED_BY";
    static final String GENERATED_ON_PARM_NAME = "GENERATED_ON";
    static final String TEMPLATE_LOCATION_PARAM_NAME = "TEMPLATE_LOCATION";

    public enum ReportCategory {
        SERVER , DATABASE
    }

    public enum ReportType {
        SUMMARY (0, "Summary Report") , BASELINE(1, "Baseline Report");
        private int id;
        private String value;

        ReportType(int id, String value) {
            this.id = id;
            this.value = value;
        }

        public int id() {
            return id;
        }        
        
        public String value() {
            return value;
        }        

    }

    public enum ExportReportType {
        PDF , CSV
    }

	@Autowired
    private DataSource dataSource;

	@Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ReportTemplateService reportTemplateService;
	
    public String generateReport() {
        try {
            //generateSrvReport();
            generateDbReport("user1", ZonedDateTime.now());
            return "done";
        }
        catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private void generateDbReport(final String generatedBy, final ZonedDateTime generatedOn) throws Exception {
        final String id = "out"; 
        final LocalDate reportDate = LocalDate.now();
        final ReportType reportType = ReportType.BASELINE;
        //final ReportType reportType = ReportType.SUMMARY;
        
        final Resource result1 = generateRecReport(id, ReportCategory.DATABASE, reportType, reportDate, ExportReportType.CSV, generatedBy, generatedOn);
        System.out.println("Report successfully generated path= " + result1.toString());

        final Resource result2 = generateRecReport(id, ReportCategory.DATABASE, reportType, reportDate, ExportReportType.PDF, generatedBy, generatedOn);
        System.out.println("Report successfully generated path= " + result2.toString());
    }

    private String generateSrvReport(final String generatedBy, final ZonedDateTime generatedOn) throws Exception {
        final String id = "out"; 
        final LocalDate reportDate = LocalDate.now();
        final ReportType reportType = ReportType.BASELINE;
        //final ReportType reportType = ReportType.SUMMARY;
        
        final Resource result1 = generateRecReport(id, ReportCategory.SERVER, reportType, reportDate, ExportReportType.CSV, generatedBy, generatedOn);
        System.out.println("Report successfully generated path= " + result1.toString());

        final Resource result = generateRecReport(id, ReportCategory.SERVER, reportType, reportDate, ExportReportType.PDF, generatedBy, generatedOn);
        return "Report successfully generated @path= " + result.toString();
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------
    
    private Path getReportDataDirectory() throws IOException { // mock
        final Path result =  Paths.get("out");
        if (Files.notExists(result)) {
            Files.createDirectories(result);
        }
        return result; 
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------
    public Resource generateRecReport(final String reportId, final ReportCategory category, final ReportType reportType,
            final LocalDate reportDate, final ExportReportType exportType, final String generatedBy, final ZonedDateTime generatedOn) throws Exception {
        Assert.notNull(reportDate, "The report date must be not null");
        Assert.notNull(exportType, "The type must be not null");
        Assert.notNull(category, "The category must be not null");
    
        final Path out = getReportDataDirectory().resolve(reportId + "." + exportType.name().toLowerCase());
        final JRVirtualizer virtualizer = reportTemplateService.getVirtualizer();
        
        final String templateName;
        final Map<String, Object> metrics;
        switch (category) {
            case SERVER:
                metrics = queryForSrvRecMetrics(reportDate);
                templateName = exportType == ExportReportType.PDF 
                    ? RECONCILIATION_SRV_TEMPLATE_NAME : RECONCILIATION_SRV_DAT_TEMPLATE_NAME;
                break;
            default:    
                metrics = queryForDbRecMetrics(reportDate);
                templateName = exportType == ExportReportType.PDF 
                    ? RECONCILIATION_DB_TEMPLATE_NAME : RECONCILIATION_DB_DAT_TEMPLATE_NAME;
                break;
        }

        // parameters
        final Map<String, Object> params = newHashMap();
        params.put(REPORT_MODE_PARM_NAME, reportType == ReportType.BASELINE ? 1 : 0); // hist->1/24h-0
        params.put(REPORT_DATE_PARM_NAME, java.sql.Date.valueOf(reportDate));
        params.put(REPORT_NAME_PARM_NAME, reportType.value());
        params.put(GENERATED_BY_PARM_NAME, generatedBy);
        params.put(GENERATED_ON_PARM_NAME, generatedOn);
        params.put(TEMPLATE_LOCATION_PARAM_NAME, reportTemplateService.getTemplateLocation().toFile().getAbsolutePath());
        if (virtualizer != null) {
            params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        }
        // add metrics..
        ofNullable(metrics).orElse(emptyMap()).entrySet().stream().forEach( m -> {
            if (log.isTraceEnabled()) {
                log.trace("Metric: {}", m.toString());
            }
            params.put(m.getKey(), m.getValue());
        });
    
        final JasperReport jasperReport = reportTemplateService.loadTemplate(templateName);
    
        // fill the report:
        final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource.getConnection());        
    
        // exporting
        final File file = out.toFile();
        switch (exportType) {
            case PDF:
                reportTemplateService.generatePdf(jasperPrint, file);
                break;
            case CSV:
                reportTemplateService.generateCsv(jasperPrint, file); 
                break;
            default:
                throw new UnsupportedOperationException(exportType.name());
        }
        return new FileSystemResource(file);
    }
    
    private Map<String, Object> queryForRecMetrics(final String sql,  final LocalDate reportDate) {
        return jdbcTemplate.queryForList(sql, reportDate).stream().collect(toMap(e -> (String) e.get("key"), e -> e.get("value")));
    }

    private Map<String, Object> queryForDbRecMetrics(final LocalDate reportDate) {
      //final String sql = DB_RECONCILIATION_METRICS_QUERY;
      final String sql = "select nvl(metric_name,'-')||'_'||nvl(entity_type,'-')||'_'||nvl(environment,'-')||'_'||nvl(gxp,'-') key, value, ? report_date from test_db_m_am";
      return queryForRecMetrics(sql, reportDate);
    }
    
    private Map<String, Object> queryForSrvRecMetrics(final LocalDate reportDate) {
        //final String sql = SRV_RECONCILIATION_METRICS_QUERY;
        final String sql = "select nvl(metric_name,'-')||'_'||nvl(entity_type,'-')||'_'||nvl(environment,'-')||'_'||nvl(gxp,'-') key, value, ? report_date from test_srv_m_am";
        return queryForRecMetrics(sql, reportDate);
    }

}
