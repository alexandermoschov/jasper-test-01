package com.sft.report;

import java.io.PrintWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.sft.report.service.EmpReportService;
import com.sft.report.service.ReportService;

/**
 * java -jar jasper-report-test-0.0.1-SNAPSHOT.jar generate
 * 
 * */
@SpringBootApplication(scanBasePackages = "com.sft")
public class JrApp implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(JrApp.class);

    @Autowired
    private ReportService reportService;

    @Autowired
    private EmpReportService empReportService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Run command {}", args.getNonOptionArgs());
        final PrintWriter out = new PrintWriter(System.out);
        try {
            final List<String> nArgs = args.getNonOptionArgs();

            if (nArgs.contains("generate")) {
                out.println(reportService.generateReport());
            } 
            else if (nArgs.contains("generate-emp")) {
                out.println(empReportService.generateReport());
            } 
            else {
                help();
            }

        }
        catch (IllegalArgumentException e) {
            printErrorMessage("Missing argument \"" + e.getMessage() + "\" .");
            System.exit(0);
        }
        out.flush();
    }

    private void help() throws Exception {
        final PrintWriter out = new PrintWriter(System.out);
        out.print("usage: java -jar jrapp.jar <method>");
        out.flush();
    }

    private void printErrorMessage(String message) {
        final PrintWriter out = new PrintWriter(System.out);
        out.println("ERROR: " + message);
        out.println();
        out.flush();
    } 

    public static void main(String[] args) throws Exception {
        new SpringApplicationBuilder(JrApp.class).web(WebApplicationType.NONE).bannerMode(Mode.CONSOLE).logStartupInfo(true).run(args);
    }    
}
